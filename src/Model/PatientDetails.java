package Model;

public class PatientDetails {

	public PatientProfile patientProfile;

	public PatientProfile getPatientProfile() {
		return patientProfile;
	}

	public void setPatientProfile(PatientProfile patientProfile) {
		this.patientProfile = patientProfile;
	}


	public PatientDetails(PatientProfile patientProfile,PatientHistory patientHistory) {
		this.patientProfile = patientProfile;		
	}
	
	public PatientDetails() {
		
	}
}
