package Model;

import Misc.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LogInModel {

    Connection connection;
    public LogInModel() {
        connection = DBConnection.Connecter();
        if(connection==null) {
          //  System.exit(1);
        }
    }

    public boolean isDBConnected() {
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public boolean isLogin(int user, String pass,String UserTyp) throws SQLException {
        PreparedStatement preparedStatement= null;
        ResultSet resultSet= null;
        String query = null;
        if(UserTyp.equals("Patient")) {
            query=  "select * from PatientProfile where PatientID = ? and Password = ?";
        }else if(UserTyp.equals("doc")){
            query = "select * from Login_Doctor where UserNum = ? and Password = ?";
        }else if(UserTyp.equals("attend")){
        query = "select * from Login_Attendent where UserNum = ? and Password = ?";
    }

        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, user);
            preparedStatement.setString(2, pass);

            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()) {
                return true;
            }else {
                return false;
            }
        }catch(Exception e) {
            return false;
        } finally {
            preparedStatement.close();
            resultSet.close();
        }
    }
}
