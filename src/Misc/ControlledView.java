package Misc;

import Controllers.ViewsController;

public interface ControlledView {

    void setViewParent(ViewsController viewsController);
}
