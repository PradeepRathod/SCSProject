package Misc;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Util {

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	 
	    BigDecimal bd = new BigDecimal(Double.toString(value));
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

	public static String screen1ID = "main";
	public static String screen1File = "/Screens/HomePage.fxml";
	public static String screen2ID = "PatientLogin";
	public static String screen2File = "/Screens/PatientLogin.fxml";
	public static String screen3ID = "AttendantLogin";
	public static String screen3File = "/Screens/AttendantLogin.fxml";
	public static String screen4ID = "DoctorLogin";
	public static String screen4File = "/Screens/DoctorLogIn.fxml";
	public static String screen5ID = "DoctorInputScreen";
	public static String screen5File = "/Screens/DoctorScreen.fxml";
	public static String screen6ID = "PatientManualMode";
	public static String screen6File = "/Screens/PatientManual.fxml";
	public static String screen7ID = "PatientAutomaticMode";
	public static String screen7File = "/Screens/PatientAutomatic.fxml";
	public static String screen8ID = "AttendantScreen";
	public static String screen8File = "/Screens/AttendentScreen.fxml";

}
