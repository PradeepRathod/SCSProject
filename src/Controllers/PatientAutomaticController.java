package Controllers;

import Misc.ControlledView;
import Misc.DBCaller;
import Misc.Util;
import Model.PatientHistory;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class PatientAutomaticController extends Thread implements Initializable, ControlledView {
	public int  start =0,startValue=10,weight,ins_disp=10,gluc_disp = 10,
			PatientID = 1, DBInsertInterval = 10, i=1, DisplayTextcounter = 0;

	public double   Curr_BGL=110,Val_Insulin=0,Val_Meal,Val_Bfast=0,Val_Lunch=0,dinner_Temp=0,mealTemp,
			insTemp=0,Ins_Pump_Val=0,Ins_ref,Total_Ins_Pump_Val=0,InsulinProgressRef=1,batteryVal=1,
			Gluc_Pump_Val=0,gluc_ref,Total_Gluc_Pump_Val=0,GlucagonProgressRef=1,counter=1.0,
			physicalActivities,physicalTemp,glucagonDose,gDoseTemp;


	public String SetText = "";

	@FXML
	public Button
			bfastBtn,
			lunchBtn,
			dinnerBtn,
			Phys_Acts,
			ONBtn,
			OFFBtn,
			Insulin_Bank,
			Glucagon_Bank,
			Battery_Btn,
			goBack;

	@FXML
	private ProgressBar pb = new ProgressBar();
	@FXML
	private ProgressBar pb_glucagon = new ProgressBar();
	@FXML
	private ProgressBar pb_battery = new ProgressBar();

	@FXML
	private Label reqIns, DisplayText;

	@FXML
	private TextField   physicalText,
			bglValue;

	public boolean  mealBool = false,insBool=false,mealTime = false,physicalBool = false,notify_ON = false,
			moreBGL= false;
	@FXML
	private LineChart<String, Number> lineChart;
	XYChart.Series<String, Number> actualValueSeries = new XYChart.Series<>();
	XYChart.Series<String, Number> InsulinThreshold = new XYChart.Series<>();
	XYChart.Series<String, Number> GlucagonThreshold = new XYChart.Series<>();

	ViewsController myController;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DisplayText.setWrapText(true);
	}

	Connection connection;

	public void goBack(ActionEvent event) throws IOException  {
		myController.setView(Util.screen2ID);
	}
	// this turns on the system
	public void sensorON() {
		start = 1;
		pb.setProgress(1.0);
		startMethod();
	}

	public void startMethod() {
		try {
			startTask();
			// this pauses the system
			OFFBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					start =0;
					System.out.println("SENSOR TURNED OFF");
				}
			});
			// this will reduce the glucose levels by the physical activities
			Phys_Acts.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					physicalBool = true;
					physicalActivities = Double.parseDouble(physicalText.getText())*2;
					physicalTemp = physicalActivities*5/40;

					glucagonDose=((Curr_BGL-108)+physicalActivities);
					gDoseTemp = glucagonDose/20;

					gluc_ref= glucagonDose;
				}
			});
			// breakfast will take input values from the text box and evaluate and start acting on the body
			bfastBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					insBool = true;
					mealTime = true;
					mealBool = true;
					Val_Bfast = 60;     // 60 gms
					Val_Meal = Val_Bfast*3; // 60 * 5 mg/dL glucose value terms
					mealTemp = Val_Meal/20;
					Val_Insulin = Val_Meal; // 60*5 mg/dL glucose value terms
					Ins_ref= Val_Insulin;


					double itemp=insTemp, iValue = Val_Insulin, min=0,curBgl=Curr_BGL,mtemp=mealTemp,mValue=Val_Meal;
					while(curBgl>108) {
						itemp = iValue/25;
						iValue = iValue - itemp;
						if(iValue<0.5) {
							break;
						}
						mtemp = mValue/20;
						mValue = iValue - mtemp;
						curBgl=curBgl*0.9995+mtemp-itemp;
						min++;
					}
					DecimalFormat f = new DecimalFormat("##.000");
				}
			});
			// lunch will take input values from the text box and evaluate and start acting on the body
			lunchBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) {
					insBool = true;
					mealTime = true;
					mealBool = true;
					Val_Lunch = 45;     // 60 gms
					Val_Meal = Val_Lunch * 3; // 60 * 5 mg/dL glucose value terms
					mealTemp = Val_Meal / 20;
					Val_Insulin = Val_Meal; // 60*5 mg/dL glucose value terms
					Ins_ref = Val_Insulin;

					double itemp = insTemp, iValue = Val_Insulin, min = 0, curBgl = Curr_BGL, mtemp = mealTemp, mValue = Val_Meal;
					while (curBgl > 108) {
						itemp = iValue / 25;
						iValue = iValue - itemp;
						if (iValue < 0.5) {
							break;
						}
						mtemp = mValue / 20;
						mValue = iValue - mtemp;
						curBgl = curBgl * 0.9995 + mtemp - itemp;
						min++;
					}
					DecimalFormat f = new DecimalFormat("##.000");
				}
			});
			// dinner will take input values from the text box and evaluate and start acting on the body
			dinnerBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					insBool = true;
					mealTime = true;
					mealBool = true;
					dinner_Temp = 50;     // 60 gms
					Val_Meal = dinner_Temp*3; // 60 * 5 mg/dL glucose value terms
					mealTemp = Val_Meal/20;
					Val_Insulin = Val_Meal; // 60*5 mg/dL glucose value terms
					Ins_ref= Val_Insulin;

					double itemp=insTemp, iValue = Val_Insulin, min=0,curBgl=Curr_BGL,mtemp=mealTemp,mValue=Val_Meal;
					while(curBgl>108) {
						itemp = iValue/25;
						iValue = iValue - itemp;
						if(iValue<0.5) {
							break;
						}
						mtemp = mValue/20;
						mValue = iValue - mtemp;
						curBgl=curBgl*0.9995+mtemp-itemp;
						min++;
					}
					DecimalFormat f = new DecimalFormat("##.000");
					//projectedTime.setText((f.format(min/60)));
				}
			});
			Insulin_Bank.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					InsulinProgressRef = 1.0;
					pb.setProgress(InsulinProgressRef);
				}
			});
			Glucagon_Bank.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					GlucagonProgressRef=1.0;
					pb_glucagon.setProgress(GlucagonProgressRef);
				}
			});
			Battery_Btn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event)
				{
					batteryVal = 1.0;
					pb_battery.setProgress(batteryVal);
				}
			});

		}
		catch(Exception e) {
			System.out.println();
		}
	}

	public void startTask() {
		// these set the chart properties
		actualValueSeries = new XYChart.Series<>();
		actualValueSeries.setName("Variation of levels with Time");
		InsulinThreshold.setName("Insulin Threshold");
		GlucagonThreshold.setName("Glucagon Threshold");
		lineChart.getData().add(actualValueSeries);
		lineChart.getData().add(InsulinThreshold);
		lineChart.getData().add(GlucagonThreshold);
		lineChart.setAnimated(true);
		lineChart.getXAxis().setTickLabelsVisible(true);
		//lineChart.getYAxis().setAutoRanging(true);
		lineChart.setCreateSymbols(false);

		Runnable task = new Runnable()
		{
			public void run()
			{
				runTask();
			}
		};


		// Run the task in a background thread
		Thread backgroundThread = new Thread(task);
		// Terminate the running thread if the application exits
		backgroundThread.setDaemon(true);
		// Start the thread
		backgroundThread.start();
		//		prepareTimeline();

	}

	public void runTask() {
		try {
			while(start==1) {
				if(start==0) {
					break;
				}      //This is the code that executes when the sensor Turns ON.
				else {


					// if BF or Lunch or Dinner is consumed --> say breakfast
					if(mealTime==true) {
						//if both meal and insulin are given inbetween or clashes by any chance
						if(mealBool==true && insBool==true) {
							if(Curr_BGL>160){
								moreBGL=true;
							}
							// if still meal is remaining to disolve
							if(Val_Meal>0){mealTemp = Val_Meal/20; Val_Meal = Val_Meal -mealTemp;}else {mealTemp = 0;}
							// if still insulin is remaining to disolve
							if(moreBGL==true){
								if(Val_Insulin>0)   {insTemp=Val_Insulin/25; Val_Insulin = Val_Insulin - insTemp;}  else {insTemp=0;}
							}
							Curr_BGL=Curr_BGL*0.9999+mealTemp-insTemp;
							if(Val_Meal<=0 && Val_Insulin <= 0) {
								mealTime = false;
								moreBGL = false;
							}
						}
					}// if physical activities happen the glucose level starts dropping
					else if(physicalBool==true) {
						if(physicalActivities>0){
							physicalTemp = physicalActivities*5/40;
							physicalActivities = physicalActivities - physicalTemp;
						}else {physicalTemp=0;}
						if(glucagonDose>0){
							System.out.println("in if......."+glucagonDose);
							gDoseTemp = glucagonDose/20;
							glucagonDose = glucagonDose - gDoseTemp;
						}else{ gDoseTemp =0;}
						Curr_BGL=Curr_BGL*0.9999+gDoseTemp-physicalTemp;
						if(physicalTemp<=0 && gDoseTemp<=0){
							physicalBool =	false;
						}
					}
					// if nothing is given given or taken, and when the body is functioning normally
					else {
						Curr_BGL= Curr_BGL*0.999756;
						// if bgl level exceeds 108, then it reduces by 0.14% of the current one.
						if(Curr_BGL>108) {
							Curr_BGL= Curr_BGL*0.99855;
						}// if bgl level falls bellow 90, then it increases by 6.9% of the current one.
						else if(Curr_BGL<90) {
							Curr_BGL= Curr_BGL*1.06923;
						}
						else if(Curr_BGL<108 && Curr_BGL>90) {
							Curr_BGL= Curr_BGL*0.99855;
						}
					}

					DecimalFormat f = new DecimalFormat("##.000");
					Platform.runLater(new Runnable()
					{
						@Override
						public void run()
						{
							//System.out.println(f.format(Curr_BGL));
						}
					});
					// This makes the graph of the obtained glucose values
					Data<String,Number> CurrentData = new XYChart.Data<String, Number>(LocalDateTime.now().toString().substring(11, 19), Curr_BGL);
					actualValueSeries.getData().add(CurrentData);
					InsulinThreshold.getData().add(new XYChart.Data<String, Number>(LocalDateTime.now().toString().substring(11, 19), 180));
					GlucagonThreshold.getData().add(new XYChart.Data<String, Number>(LocalDateTime.now().toString().substring(11, 19), 40));

					// This maintains exactly 25 points on the X-axis
					if(actualValueSeries.getData().size() >25)
					{
						actualValueSeries.getData().remove(0, 1);
						InsulinThreshold.getData().remove(0, 1);
						GlucagonThreshold.getData().remove(0, 1);
					}

					// This turns on the alarm when the glucose Level falls in the danger zone of Curr_BGL <= 40 or Curr_BGL >= 180 mg/dL
					if(Curr_BGL>=180 || Curr_BGL <= 40) {
						notify_ON= true;

					}
					int DBCounter = 0;
					if(DBCounter % DBInsertInterval==0) {
						System.out.println("in if........."+DBCounter);
						PatientHistory patientHistory = new PatientHistory(Curr_BGL,PatientID);
						DBCaller.InsertPatientHistory(patientHistory);
					}
					DBCounter++;
					// this sets the pump to the exact insulin remaining
					if(insBool==true && Val_Insulin >0 ) {
						// to increase the capacity of the tank divide by a factor
						Ins_Pump_Val=(((Ins_ref-Val_Insulin)/50)/ins_disp)/2;
						if(Val_Insulin==0) {
							Total_Ins_Pump_Val=Total_Ins_Pump_Val+Ins_Pump_Val;
						}
						pb.setProgress(InsulinProgressRef-(Ins_Pump_Val-Total_Ins_Pump_Val)/100);
						InsulinProgressRef = InsulinProgressRef-(Ins_Pump_Val-Total_Ins_Pump_Val)/100;
					}
					// this sets the pump to the exact glucagon remaining
					if(physicalBool==true && glucagonDose >0 ) {
						// to increase the capacity of the tank divide by a factor
						Gluc_Pump_Val=(((gluc_ref-glucagonDose)/50)/gluc_disp)/2;

						if(glucagonDose==0) {
							Total_Gluc_Pump_Val=Total_Gluc_Pump_Val+Gluc_Pump_Val;
						}
						//  pb_glucagon.setProgress((GlucagonProgressRef-(Gluc_Pump_Val-Total_Gluc_Pump_Val)/100));
						GlucagonProgressRef = GlucagonProgressRef-(Gluc_Pump_Val-Total_Gluc_Pump_Val)/100;
						pb_glucagon.setProgress(GlucagonProgressRef);

					}
					batteryVal = 1 - (1-InsulinProgressRef)/20-(1-GlucagonProgressRef)/20 -counter/1200;
					pb_battery.setProgress(batteryVal);
					System.out.println("-------------------------------------------");
					System.out.println("InsulinProgressRef = "+ (1-InsulinProgressRef)/10);
					System.out.println("GlucagonProgressRef = "+ (1-GlucagonProgressRef)/10);
					System.out.println("counter = "+ counter/600);
					System.out.println("batteryVal = "+ batteryVal);
					System.out.println("-------------------------------------------");

					//	projectedTime.setText((f.format(GlucagonProgressRef/2)));
					bglValue.setText((f.format(Curr_BGL)));//f.format(x)

					Thread.sleep(500);
				}
				counter++;
			}
		}
		catch(Exception e) {
			System.out.println("exception is.."+e);
		}
	}

	@Override
	public void setViewParent(ViewsController screenPage) {
		// TODO Auto-generated method stub
		myController =screenPage;
	}
	public void SetDisplayText() {


		switch(i)
		{
			case(1):
				SetText = "1.	Lose extra weight. Moving toward a healthy weight helps control blood sugars. Your doctor, a dietitian, and a fitness trainer can get you started on a plan that will work for you.";
				i++;
				break;
			case(2):
				SetText = "2.	Check your blood sugar level at least twice a day. Is it in the range advised by your doctor? Also, write it down so you can track your progress and note how food and activity affect your levels.";
				i++;
				break;
			case(3):
				SetText = "3.	Get A1c blood tests to find out your average blood sugar for the past 2 to 3 months. Most people with type 2 diabetes should aim for an A1c of 7% or lower. Ask your doctor how often you need to get an A1c test.";
				i++;
				break;
			case(4):
				SetText = "4.	Track your carbohydrates. Know how many carbs you’re eating and how often you have them. Managing your carbs can help keep your blood sugar under control. Choose high-fibre carbs, such as green vegetables, fruit, beans, and whole grains.";
				i++;
				break;
			case(5):
				SetText = "5.	Control your blood pressure, cholesterol, and triglyceride levels. Diabetes makes heart disease more likely, so keep a close eye on your blood pressure and cholesterol. Talk with your doctor about keeping your cholesterol, triglycerides, and blood pressure in check. Take medications as prescribed.";
				i++;
				break;
			case(6):
				SetText = "6.	Keep moving. Regular exercise can help you reach or maintain a healthy weight. Exercise also cuts stress and helps control blood pressure, cholesterol, and triglyceride levels. Get at least 30 minutes a day of aerobic exercise 5 days a week. Try walking, dancing, low-impact aerobics, swimming, tennis, or a stationary bike. Start out more slowly if you aren't active now. You can break up the 30 minutes -- say, by taking a 10-minute walk after every meal. Include strength training and stretching on some days, too.";
				i++;
				break;
			case(7):
				SetText = "7.	Catch some ZZZs. When you’re sleep-deprived, you tend to eat more, and you can put on weight, which leads to health problems. People with diabetes who get enough sleep often have healthier eating habits and improved blood sugar levels.";
				i++;
				break;
			case(8):
				SetText = "8.	Manage stress. Stress and diabetes don't mix. Excess stress can elevate blood sugar levels. But you can find relief by sitting quietly for 15 minutes, meditating, or practicing yoga.";
				i++;
				break;
			case(9):
				SetText = "9.	See your doctor. Get a complete check-up at least once a year, though you may talk to your doctor more often. At your annual physical, make sure you get a dilated eye exam, blood pressure check, foot exam, and screenings for other complications such as kidney damage, nerve damage, and heart disease.";
				i=1;
				break;

		}
		//System.out.println(SetText);
	/*	Platform.runLater(new Runnable() {
			@Override public void run() {
				//DisplayText.setText(SetText);
			}
		}); */


	}
}

	


