package Controllers;


import Misc.ControlledView;
import Misc.DBCaller;
import Misc.Main;
import Misc.Util;
import Model.LogInModel;
import Model.PatientProfile;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class PatientController implements Initializable, ControlledView {
	
	public LogInModel loginModal = new LogInModel();
	@FXML	private TextField UserNameField,PasswordField;
	@FXML	private Label connStatus;
	@FXML	private Button Login,goBack;

	public static int PatientID = 0;
	ViewsController controller;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
			if(loginModal.isDBConnected()) {
				connStatus.setText("Status: connected, You are ready to Log In");
			System.out.println("connnnnn");

		}
		else
		{
			connStatus.setText("Status : Disconnected, You can not Log In");
		}
	}

	public void login(ActionEvent event) throws  Exception {

		try {
			if(loginModal.isLogin(Integer.parseInt(UserNameField.getText()), PasswordField.getText(),"Patient")) {
				connStatus.setText("User name & Password is correct");
				PatientID = Integer.parseInt(UserNameField.getText()) ;

				PatientProfile config =  DBCaller.checkPatientMode(PatientController.PatientID);
				System.out.println("config.isManual()....."+config.isManual());
				if(!config.isManual()) {
					controller.setView(Util.screen6ID);
				}else {
					controller.setView(Util.screen7ID);
				}

			}
			else {
				connStatus.setText("User name & Password is incorrect");

			}
		} catch (SQLException e) {
			connStatus.setText("User name & Password is incorrect");

		}
		}
	
	public void goBack(ActionEvent event) throws IOException  {
		controller.setView(Util.screen1ID);
	}
	@Override
	public void setViewParent(ViewsController viewPage) {
		// TODO Auto-generated method stub
		controller =viewPage;
	}
	

	}




