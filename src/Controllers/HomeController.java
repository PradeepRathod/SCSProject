package Controllers;

import Misc.ControlledView;
import Misc.Main;
import Misc.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;

import java.io.IOException;

public class HomeController implements ControlledView {

	ViewsController controller;
	@FXML
	private Button DoctorLoginBtn,AttendLoginBtn,PatientLoginBtn;


	
	public void goToDoctorLogin(ActionEvent event) throws IOException  {
		controller.setView(Util.screen4ID);//load doctor log-in screen
       // FXMLLoader loader = new FXMLLoader(getClass().getResource(Util.screen5ID));
		//UserLabel.setText("Doctor LogIn");

}
public void goToPatientLogin(ActionEvent event) throws IOException  {
	//AnchorPane pane = FXMLLoader.load(getClass().getResource("LogInScreen.fxml"));
//	rootpane.getChildren().setAll(pane);
//		ispatient = true;
	controller.setView(Util.screen2ID); //load patient log-in screen
	//myController.setScreen(Util.screen2ID);

	}

	public void goToAttendLogin(ActionEvent event) throws IOException  {
		//AnchorPane pane = FXMLLoader.load(getClass().getResource("LogInScreen.fxml"));
//	rootpane.getChildren().setAll(pane);
//		ispatient = true;
		controller.setView(Util.screen3ID); //load patient log-in screen
		//myController.setScreen(Util.screen2ID);

	}
    @Override
    public void setViewParent(ViewsController viewPage) {
        controller =viewPage;
    }

}
