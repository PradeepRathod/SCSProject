package Controllers;


import Misc.ControlledView;
import Model.LogInModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class AttendentLoginController implements Initializable, ControlledView {
	
	public LogInModel loginModal = new LogInModel();
	ViewsController controller;
	@FXML
	private TextField txtUserName,txtPassword;
	@FXML
	private Label isConnected;
	@FXML
	private Button Login,goBack;
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("loginModal.isDBConnected()...");
		if(loginModal.isDBConnected()) {
			isConnected.setText("Connected");
		}
		else
		{
			isConnected.setText("Not Connected");
		}
		
	}
	public void login(ActionEvent event) {
		controller.setView("screen8ID");
	try {
			if(loginModal.isLogin(Integer.parseInt(txtUserName.getText()), txtPassword.getText(),"attend")) {
				isConnected.setText("User name & Password is correct");
				controller.setView("screen8ID");
			}
			else {
				isConnected.setText("User name & Password is incorrect");
			}
		} catch (SQLException e) {
			isConnected.setText("User name & Password is incorrect");
			
		}
	}
	
	public void goBack(ActionEvent event) throws IOException  {
		controller.setView("screen1ID");
	}
	@Override
	public void setViewParent(ViewsController viewPage) {
		// TODO Auto-generated method stub
		controller =viewPage;
	}

}

