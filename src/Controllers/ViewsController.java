package Controllers;

import Misc.ControlledView;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.util.HashMap;

public class ViewsController extends StackPane {

    private HashMap<String, Node> views = new HashMap<>();

    public ViewsController() {
        super();
    }

    public boolean loadView(String name, String resource) {
        try {
            FXMLLoader myLoader;
            myLoader = new FXMLLoader(getClass().getResource(resource));
            Parent loadView = (Parent) myLoader.load();
            ControlledView myViewController = ((ControlledView) myLoader.getController());
            myViewController.setViewParent(this);
            addView(name, loadView);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage()+resource);
            return false;
        }
    }

    public boolean setView(final String name) {
        if (views.get(name) != null) {   //screen loaded
            final DoubleProperty opacity = opacityProperty();

            if (!getChildren().isEmpty()) {    //if there is more than one screen
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent t) {
                                getChildren().remove(0);                    //remove the displayed screen
                                getChildren().add(0, views.get(name));     //add the screen
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(800), new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0)));
                fade.play();

            } else {
                setOpacity(0.0);
                getChildren().add(views.get(name));       //no one else been displayed, then just show
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(2500), new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else {
            System.out.println("screen hasn't been loaded!!! \n");
            return false;
        }
    }


    public boolean unloadView(String name) {
        if (views.remove(name) == null) {
            System.out.println("Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }

    public void addView(String name, Node screen) {
        views.put(name, screen);
    }

    //Returns the Node with the appropriate name
    public Node getView(String name) {
        return views.get(name);
    }
}
