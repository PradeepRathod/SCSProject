package Controllers;

import Misc.ControlledView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;

//implements Initializable
public class AttendentController implements ControlledView {
	ObservableList<Integer> IDList = FXCollections.observableArrayList();
	ViewsController controller;
	 @FXML	 TextField txtpatientName = new TextField(); 
	 @FXML	TextField txtpatientAge = new TextField();
	 @FXML	 TextField txtpatientAddress = new TextField(); 
	 @FXML	TextField txtpatientAdditionalDrugs = new TextField();
	 @FXML	 ComboBox<Integer> cmbPatientID = new ComboBox<Integer>(); 
	 @FXML	TextField txtpatientContactNo = new TextField();
	 @FXML	 Button btnLoad = new Button();
	 @FXML	 Button btnSave = new Button();
	 @FXML	 Button btnEnableAddtion = new Button();
	 @FXML	 Button btnSaveNewPatient = new Button();
	 @FXML	 Button btnEditDeviceConfiguration = new Button();
	 @FXML	 Button btnSaveDeviceConfiguration = new Button();
	 @FXML	 DatePicker dtNextAppointment = new DatePicker();
	 @FXML	 RadioButton ManualMode = new RadioButton();
	 @FXML   Button  Login;
	 @FXML   private TextField txtUserName,txtPassword;
	 @FXML   private Label isConnected;
	 @FXML   private Button goBack;
	 @FXML   private void initialize()
	 {
			//cmbPatientID.setItems(DBCaller.selectPatientID("AllPatient"));

	 }
	
	 public void goBack(ActionEvent event) throws IOException  {
		 controller.setView("screen3ID");
		}

	public void LoadPatientProfile() 
	{

			ManualMode.setSelected(true);			
			txtpatientName.setEditable(false);
			txtpatientAge.setEditable(false);
			txtpatientAddress.setEditable(false);
			txtpatientAdditionalDrugs.setEditable(false);		
			txtpatientContactNo.setEditable(false);
			btnSaveNewPatient.setVisible(false);
			btnSave.setVisible(false);
			
	}
	
	public void btnAddNewPatient() {

	}
	
		
	public void OnPatientIDSelect()
	{
		LoadPatientProfile();

	}
	
	public void EnableAddtion() {
		txtpatientName.clear();
		txtpatientAge.clear();
		txtpatientAddress.clear();
		txtpatientAdditionalDrugs.clear();		
		txtpatientContactNo.clear();
		txtpatientName.setEditable(true);
		txtpatientAge.setEditable(true);
		txtpatientAddress.setEditable(true);
		txtpatientAdditionalDrugs.setEditable(true);		
		txtpatientContactNo.setEditable(true);
	}
	
	@Override
	public void setViewParent(ViewsController viewsPage) {
		controller =viewsPage;
		
	}
	
	}
	
	

