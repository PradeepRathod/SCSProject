package Controllers;

import Misc.ControlledView;
import Misc.Util;
import Model.LogInModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class DoctorLoginController implements Initializable, ControlledView {
	public LogInModel doctor = new LogInModel();
	ViewsController controller;
	@FXML
	private TextField txtUserName,txtPassword;
	@FXML
	private Label isConnected;
	@FXML
	private Button Login,goBack;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		if(doctor.isDBConnected()) {
			isConnected.setText("Connected");
	}
		else
		{
			isConnected.setText("Not Connected");
		}
		
	}
	public void login(ActionEvent event) throws Exception {
		try {
			String password = txtPassword.getText();
			System.out.println(password);
			if(doctor.isLogin(Integer.parseInt(txtUserName.getText()), password,"doc")) {
				isConnected.setText("User name & Password is correct");
				controller.setView(Util.screen5ID);
			}
			else {
				isConnected.setText("User name & Password is incorrect");
			}
		} catch (SQLException e) {
			isConnected.setText("User name & Password is incorrect");
			
		}
		//controller.setView(Util.screen5ID);
	}
	
	public void goBack(ActionEvent event) throws IOException  {
		controller.setView("screen1ID");
	}
	@Override
	public void setViewParent(ViewsController viewsPage) {
				controller =viewsPage;
	}
	

}
