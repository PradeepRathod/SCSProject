package Controllers;

import Misc.ControlledView;
import Misc.DBCaller;
import Model.PatientHistory;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Font;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ResourceBundle;


public class PatientManualController extends Thread implements Initializable, ControlledView {
	//implements Initializable for progress bar
	public int  start =0,
				startValue=10,
				ins_disp=10,
				gluc_disp = 10,
				counter=1, 
				PatientID = 1, DBInsertInterval = 10;
	
	public double   Curr_BGL=110,Val_Insulin=0,Val_NextIns =0,Val_Meal,
					lnTemp=0,dinTemp=0,mealTemp=0,insTemp=0,req_Ins=0,CHO_ins_dose,Ins_Pump_Val=0,
					Ins_ref,Total_Ins_Pump_Val=0,refInsPB=1,Gluc_Pump_Val=0,gluc_ref,Total_Gluc_Pump_Val=0,
					refGlucPB=1,refferenceBatery=1,xRef=0,Total_max_Dosage=0,preInsulin=0,preInsulinTemp=0,
					physicalActivities=0, physicalTemp=0,glucagonDose=0,gDoseTemp=0,weight = 100.0,
					Val_Bfast=0,Val_lunch=0,Val_dinner=0;

	//initial concentration of carbohydrates
	double A0 = 121.7;
	// rate of food digestion(Glycemic Index)
	double k1= 0.0453;
	// rate at which insulin is released
	double k2 = 0.0224;
	public static final double e = 2.71828;
	@FXML
	public Button 	ONButton,bfastBtn,lunchBtn,dinnerBtn,InsulinButn,GlucagonBtn,BatteryBtn,InjInsulin,InjGlucagon,OffButton,
					checkBFInsBtn,checkLunchInsBtn,checkDinnerInsBtn,physBtn,Cancel;
	
	@FXML
	private TextField   bFastText,LunchText,DinnerText, physicalText,
						bglValue,
						projectedTime,
						DisplayText;
	
	public boolean  mealBool = false,
					insBool=false,
					bfBool=false,
					lnBool=false,
					dinBool=false,
					extraInsBool = false,
							 ChangeSize = true,
					mealTime = false,
					breakfastTime = false, 
					lunchTime = false , 
					dinnerTime = false, 
					extraCarbsTime = false,

					checkBFInsBool = false,
					checkLunchInsBool = false,
					checkDinnerInsBool = false,
					physicalBool = false,
					glucagonBool = false,
					notify_ON = false;
	double batteryvalue = 1;
	@FXML	private ProgressBar InsulinIndicator = new ProgressBar(); //Insulin Progress bar
	@FXML	private ProgressBar GlucagonIndicator = new ProgressBar();//Glucagon Progress bar
	@FXML	private ProgressBar BatteryIndicator = new ProgressBar(); //Battery Progress Bar
	@FXML
	private Label reqIns; //it shows the required insulin
	@FXML
	private LineChart<String, Number> lineChart;//List for Linechart
	XYChart.Series<String, Number> actualValueSeries = new XYChart.Series<>();//List for Varying values in linechart
	XYChart.Series<String, Number> InsulinThreshold = new XYChart.Series<>();//list for threshold insulin values in linechart
	XYChart.Series<String, Number> GlucagonThreshold = new XYChart.Series<>();//list for threshold glucagon in linechart



	ViewsController controller;

	public void cancel(ActionEvent event) throws IOException  {
		controller.setView("screen2ID");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		DisplayText.setEditable(false);
		physicalText.setText("0.0");
		bFastText.setText("0.0");
		LunchText.setText("0.0");
		DinnerText.setText("0.0");
	}

	Connection connection;
	// this turns on the system
	public void sensorON() {
		start = 1;
		System.out.println("SENSOR TURNED ON");
		InsulinIndicator.setProgress(1.0);
		GlucagonIndicator.setProgress(1.0);
		BatteryIndicator.setProgress(1.0);

		startMethod();


	}

	public void startMethod() {
		try {
			startTask();
			// this pauses the system
			OffButton.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{      
					start =0; 
					System.out.println("SENSOR TURNED OFF");
				}
			});
			// this will reduce the glucose levels by the physical activities
			physBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{  
					if((physicalText.getText()!=null) && (physicalText.getText()!="")) {
					System.out.println("coming inside PHYSICAL ACTIVITY");
					physicalBool = true;
					physicalActivities = Double.parseDouble(physicalText.getText());
					gluc_ref= physicalActivities*3;
					physicalTemp = physicalActivities* 3/40;
					}
					else if((physicalText.getText()==null) && (physicalText.getText()=="")) {
						System.out.println("Please Enter the Physical Activies value");
					}

				}
			});
			// this will start injecting glucagon
						InjGlucagon.setOnAction(new EventHandler <ActionEvent>()
						{
							public void handle(ActionEvent event) 
							{      
								if(physicalBool==true) {
								glucagonBool = true;	
								glucagonDose=((Curr_BGL-108)+Double.parseDouble(physicalText.getText())*2);
								gDoseTemp = glucagonDose/20;
								System.out.println("glucagonDose is "+glucagonDose+" gDoseTemp is "+gDoseTemp);
								System.out.println("STARTED GLUCAGON");
								}
								else {
									System.out.println("PLEASE PRESS THE INSULIN BUTTON");
								}
							}
						});
			// breakfast will take input values from the text box and evaluate and start acting on the body
			bfastBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{      

					if((!bfastBtn.getText().equals(null)) && (!bFastText.getText().equals(""))) {
					if(insBool==false) {
						breakfastTime = true;
						bfBool=true;
						mealTime = true;
						mealBool = true;
						Val_Bfast = Double.parseDouble(bFastText.getText());     // 60 gms
						Val_Meal = Val_Bfast*3; // Val_Bfast * 3 mg/dL glucose value terms
						mealTemp = Val_Meal/20;
					if(checkBFInsBool==false) {
						Val_Insulin = Val_Meal; // Val_Bfast* 3 mg/dL glucose value terms
						Ins_ref= Val_Insulin;
						insTemp = Val_Insulin/25;
					}
					else if(checkBFInsBool==true) {
						Val_Insulin=preInsulin;
						Ins_ref= Val_Insulin;
						insTemp = Val_Insulin/25;
						preInsulin=0;
						checkBFInsBool=false;
					}
					}
					else if(insBool==true) {
						breakfastTime = true;
						bfBool=true;
						mealTime = true;
						mealBool = true;
						Val_Bfast = Val_Meal/3 + Double.parseDouble(bFastText.getText());     // 60 gms
						Val_Meal = Val_Bfast*3; // 60 * 3 mg/dL glucose value terms
						mealTemp = Val_Meal/20;
						//System.out.println("Val_Bfast --> "+Val_Bfast);
						if(checkBFInsBool==false) {
							if(Val_Insulin==0) {
								Val_Insulin=Val_Meal;
							}else{
								Val_NextIns = Val_Meal;
							}
							Ins_ref= Val_Insulin;
							insTemp = Val_Insulin/25;
						}
						else if(checkBFInsBool==true) {
							Val_Insulin=preInsulin + Val_Bfast*3;;
							Ins_ref= Val_Insulin;
							insTemp = Val_Insulin/25;
							preInsulin=0;
							checkBFInsBool=false;
						}
					}
				}
				else {
					System.out.println("Please enter a value in the Breakfast Text Area");
				}
			}
			});
			// lunch will take input values from the text box and evaluate and start acting on the body
			lunchBtn.setOnAction(new EventHandler <ActionEvent>() 
			{
				public void handle(ActionEvent event) 
				{      
					if((LunchText.getText()!=null) && (LunchText.getText()!="")) {
					if(insBool == false) {
					lunchTime = true;
					lnBool=true;
					Val_lunch = Double.parseDouble(LunchText.getText());      // 45 gms 	
					lnTemp = Val_lunch/40* 3; // rate of disolution of carbs into the blood
					mealTime = true;
					mealBool = true;
					Val_Meal = Val_lunch*3;    // Val_Meal * 3 mg/dL glucose value terms
					mealTemp = Val_Meal/20;
					if(checkLunchInsBool==false) {
						Val_Insulin = Val_lunch*3; // Val_lunch* 3 mg/dL glucose value terms
						Ins_ref= Val_Insulin;
						insTemp = Val_Insulin/25;
					}
					else if(checkLunchInsBool==true) {
						Val_Insulin=preInsulin;
						Ins_ref= Val_Insulin;
						insTemp = Val_Insulin/25;
						preInsulin=0;
						checkBFInsBool=false;
					}
					}
					else if(insBool == true) {
						lunchTime = true;
						lnBool=true;
						Val_lunch = Val_Meal/3 + Double.parseDouble(LunchText.getText());      // 45 gms 	
						lnTemp = Val_lunch/40* 3; // rate of disolution of carbs into the blood
						mealTime = true;
						mealBool = true;
						Val_Meal = Val_lunch*3;    // Val_lunch * 3 mg/dL glucose value terms
						mealTemp = Val_Meal/20;
						if(checkLunchInsBool==false) {
							//System.out.println("comes here into check bool is "+checkBFInsBool+ " and insuline is "+ Val_Insulin);
							if(Val_Insulin==0) {
								Val_Insulin=Val_Meal;
							}else{
								Val_NextIns = Val_Meal;
							}
							Ins_ref= Val_Insulin;
							insTemp = Val_Insulin/25;
						}
						else if(checkLunchInsBool==true) {
							Val_Insulin=preInsulin;
							Ins_ref= Val_Insulin;
							insTemp = Val_Insulin/25;
							preInsulin=0;
							checkBFInsBool=false;
						}
						}
					System.out.println("lunchTime is " + lunchTime);
				}
					else {
						System.out.println("Please enter value in the Lunch Text");
					}
				}
			});
			// dinner will take input values from the text box and evaluate and start acting on the body
			dinnerBtn.setOnAction(new EventHandler <ActionEvent>() 
			{
				public void handle(ActionEvent event) 
				{      
					if((!DinnerText.getText().equals(null)) && (!DinnerText.getText().equals(""))) {
					if(insBool==false) {
					dinnerTime = true;
					dinBool=true;
					Val_dinner = Double.parseDouble(DinnerText.getText());     // 30 gms 	
					dinTemp = Val_dinner/40* 3; // rate of disolution of carbs into the blood
					mealTime = true;
					mealBool = true;
					Val_Meal = Val_dinner*3;  // Val_dinner * 3 mg/dL glucose value terms
					mealTemp = Val_Meal/20;
					if(checkDinnerInsBool==false) {
						Val_Insulin = Val_dinner*3; // Val_dinner* 3 mg/dL glucose value terms
						System.out.println("comes here into check bool is "+checkBFInsBool+ " and insulin is "+ Val_Insulin);

						Ins_ref= Val_Insulin;
						insTemp = Val_Insulin/25;
					}
					else if(checkDinnerInsBool==true) {
						Val_Insulin=preInsulin;
						Ins_ref= Val_Insulin;
						insTemp = Val_Insulin/25;
						preInsulin=0;
						checkBFInsBool=false;
					}
					System.out.println("dinnerTime is " + dinnerTime);
					//					Val_Insulin = Val_Meal; // 60* 3 mg/dL glucose value terms
					//					insTemp = Val_Insulin/25;
				     }
					else if(insBool==true) {
						dinnerTime = true;
						dinBool=true;
						Val_dinner = Val_Meal/3 + Double.parseDouble(DinnerText.getText());     // 30 gms 	
						dinTemp = Val_dinner/40* 3; // rate of disolution of carbs into the blood
						mealTime = true;
						mealBool = true;
						Val_Meal = Val_dinner*3;  // Val_dinner * 3 mg/dL glucose value terms
						mealTemp = Val_Meal/20;
						if(checkDinnerInsBool==false) {
							System.out.println("comes here into check bool is "+checkBFInsBool+ " and insuline is "+ Val_Insulin);
							if(Val_Insulin==0) {
								Val_Insulin=Val_Meal;
							}else{
								Val_NextIns = Val_Meal;
							}
							Ins_ref= Val_Insulin;
							insTemp = Val_Insulin/25;
						}
						else if(checkDinnerInsBool==true) {
							Val_Insulin=preInsulin;
							Ins_ref= Val_Insulin;
							insTemp = Val_Insulin/25;
							preInsulin=0;
							checkBFInsBool=false;
						}
						System.out.println("dinnerTime is " + dinnerTime);
					     }
					}
					else {
						System.out.println("Please enter the Dinner Value in the Text Field");
					}
					}
			});
			// this will take input from the text box and then calculate required insulin
			checkBFInsBtn.setOnAction(new EventHandler <ActionEvent>() 
			{
				public void handle(ActionEvent event) 
				{      
					if((bFastText.getText()!=null) && (bFastText.getText()!="")) {
					checkBFInsBool = true;
					Val_Bfast = Double.parseDouble(bFastText.getText());
					Total_max_Dosage = weight * 0.55;
					System.out.println("coming to line 1");
					CHO_ins_dose=Val_Bfast/ins_disp;
					CHO_ins_dose=(Curr_BGL-108)/50+CHO_ins_dose;
					req_Ins = CHO_ins_dose;
					preInsulin=Val_Bfast*3+(Curr_BGL-108);
					preInsulinTemp=preInsulin/25;

					System.out.println("coming to line 2");
					DecimalFormat f = new DecimalFormat("##.000");
					if(mealTime == true) {
						reqIns.setText("Its MealTime");
					}
					else {

						System.out.println("coming to line 3");
						reqIns.setText(f.format(req_Ins));
					}
					Val_Bfast = 0; 
					CHO_ins_dose = 0;
					req_Ins = 0;

				}else {
					System.out.println("Please enter the value in the Breakfast Text Field");
				}
					}
			});
			// this will take input from the text box and then calculate required insulin
			checkLunchInsBtn.setOnAction(new EventHandler <ActionEvent>() 
			{
				public void handle(ActionEvent event) 
				{      
					if((LunchText.getText()!=null) && (LunchText.getText()!="")) {
					checkLunchInsBool = true;
					Val_lunch = Double.parseDouble(LunchText.getText());  
					Total_max_Dosage = weight * 0.55;
					CHO_ins_dose=Val_lunch/ins_disp;
					CHO_ins_dose=(Curr_BGL-108)/50+CHO_ins_dose;
					req_Ins = CHO_ins_dose;
					preInsulin=Val_lunch*3+(Curr_BGL-108);
					preInsulinTemp=preInsulin/25;
					DecimalFormat f = new DecimalFormat("##.000");
					if(mealTime == true) {
						reqIns.setText("Its MealTime");
					}
					else {
						reqIns.setText(f.format(req_Ins));
					}
					Val_lunch = 0; 
					CHO_ins_dose = 0;
					req_Ins = 0;
				}else {
					System.out.println("Please enter the Lunch Text Field");
				}
					}
			});
			// this will take input from the text box and then calculate required insulin
			checkDinnerInsBtn.setOnAction(new EventHandler <ActionEvent>() 
			{
				public void handle(ActionEvent event) 
				{     
					if((DinnerText.getText()!=null) && (DinnerText.getText()!="")) {
					checkDinnerInsBool = true;
					Val_dinner = Double.parseDouble(DinnerText.getText()); 
					Total_max_Dosage = weight * 0.55;
					CHO_ins_dose=Val_dinner/ins_disp;
					CHO_ins_dose=(Curr_BGL-108)/50+CHO_ins_dose;
					req_Ins = CHO_ins_dose;
					preInsulin=Val_dinner*3+(Curr_BGL-108);
					preInsulinTemp=preInsulin/25;
					DecimalFormat f = new DecimalFormat("##.000");
					if(mealTime == true) {
						reqIns.setText("Its MealTime");
					}
					else {
						reqIns.setText(f.format(req_Ins));
					}
					Val_dinner = 0; 
					CHO_ins_dose = 0;
					req_Ins = 0;
				}else {
					System.out.println("Please enter the dinner Text Field");
				}
					}
			});
			// This will refill the Insulin tank
			InsulinButn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{      
					refInsPB = 1.0;
					InsulinIndicator.setProgress(refInsPB);
				}
			});
			GlucagonBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{      
					refGlucPB=1.0;
					GlucagonIndicator.setProgress(refGlucPB);
				}
			});
			BatteryBtn.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{   refferenceBatery = 1.0;
					BatteryIndicator.setProgress(refferenceBatery);
				}
			});
			// this will start injecting insulin when pressed
			InjInsulin.setOnAction(new EventHandler <ActionEvent>()
			{
				public void handle(ActionEvent event) 
				{   
					if(weight<200) {
					insBool = true;	
					//CHO_ins_dose=Val_dinner/ins_disp;
					CHO_ins_dose=(Curr_BGL-108)/50+CHO_ins_dose;
					req_Ins = CHO_ins_dose;
					if(Val_NextIns !=0) {
						Val_Insulin = Val_NextIns;
						Ins_ref= Val_Insulin;
						Val_NextIns=0;
					}
					
					double itemp=insTemp, iValue = Val_Insulin, min=0,curBgl=Curr_BGL,refBgl=108,mtemp=mealTemp,mValue=Val_Meal;
					while(curBgl>108) {
						itemp = iValue/25;
						iValue = iValue - itemp;
						if(iValue<0.5) {
							break;
						}
						mtemp = mValue/20;
						mValue = iValue - mtemp;
						curBgl=curBgl*0.9995+mtemp-itemp;
				 		min++;
				 		}
					System.out.println("curBgl is "+ curBgl+ "refBgl is "+ refBgl);
				 		System.out.println("Your body would be neutralized in "+ min+" iterations and it takes "+ min*10+" Muntes to finish.");
				 	
				 		 DecimalFormat f = new DecimalFormat("##.000");
				 		 projectedTime.setText((f.format(min/60)));
					}

				}
			});

		}
		catch(Exception e) {
			System.out.println();	
		}
	}

	public void startTask() {
		// these set the chart properties
		actualValueSeries = new XYChart.Series<>();
		actualValueSeries.setName("Variation of levels with Time");
		InsulinThreshold.setName("Insulin Threshold");
		GlucagonThreshold.setName("Glucagon Threshold");
		lineChart.getData().add(actualValueSeries);
		lineChart.getData().add(InsulinThreshold);
		lineChart.getData().add(GlucagonThreshold);
		lineChart.setAnimated(true);
		lineChart.getXAxis().setTickLabelsVisible(true);
		//lineChart.getYAxis().setAutoRanging(true);
		lineChart.setCreateSymbols(false);

		Runnable task = new Runnable()
		{
			public void run()
			{
				runTask();
			}
		};

		Thread backgroundThread = new Thread(task);// Run the task in a background thread

		backgroundThread.setDaemon(true);// Terminate the running thread if the application exits
		backgroundThread.start();		// Start the thread

	}

	public void runTask() {
		try {
			while(start==1) {
				if(start==0) {
					break;
				}      //This is the code that executes when the sensor Turns ON.
				else {
					 if(mealTime==true || insBool == true) {
						
						// if the breakfast was taken and there is no insulin
						if(mealBool==true && insBool == false) {
					//		System.out.println("it is in condition 1");
							// if breakfast is still not mixed in the blood
							if((Val_Meal)>0) {
								mealTemp = Val_Meal/20;
								Val_Meal= Val_Meal - mealTemp;
								Curr_BGL=Curr_BGL*0.9995+mealTemp;
							}// if breakfast is completely mixed in the blood
							else if(Val_Meal<=0) {
								mealBool= false;
							}
						}
						// if there is no breakfast to disolve and only insulin acting 
						else if(mealBool == false && insBool==true) {
					//		System.out.println("it is in condition 2 and mealBool is " +mealBool + ", insBool is "+insBool);
							
							// if there is still insulin left to act in the blood after injection
							if(Val_Insulin>0) 
							{
								insTemp=Val_Insulin/25;
								Val_Insulin = Val_Insulin - insTemp;
								Curr_BGL=Curr_BGL*0.9995-insTemp; 
							}//this will induce insulin for required carbs before the meal
							else if(Val_Insulin<=0) {
								// patient checks either of these 3 meals to check required Insulin to inject
								if(checkDinnerInsBool ||checkLunchInsBool ||checkBFInsBool ) {
									//before giving the meal if the insulin injected is still left to be disolved into the blood
									if(preInsulin>0) {
										preInsulinTemp=preInsulin/25;
										preInsulin = preInsulin - preInsulinTemp;
										Curr_BGL=Curr_BGL*0.9995-preInsulinTemp; 
									}// if everything is done the we should not enter again into this condition
								}else {
									insBool= false;
									checkDinnerInsBool= false;
									checkLunchInsBool=false;
									checkBFInsBool=false;
								}
							}  
						}// if both meal and insulin are given inbetween or clashes by any chance
						else if(mealBool==true && insBool==true) {
						//	System.out.println("it is in condition 3");
							// if still meal is remaining to disolve
							if(Val_Meal>0) {mealTemp = Val_Meal/20; Val_Meal = Val_Meal -mealTemp;}else {mealTemp = 0;}
							// if still insulin is remaining to disolve
							if(Val_Insulin>0)   {insTemp=Val_Insulin/25; Val_Insulin = Val_Insulin - insTemp;}  else {insTemp=0;}
							Curr_BGL=Curr_BGL*0.9999+mealTemp-insTemp;  


							if(Val_Meal<=0) {
								mealBool= false;

							}
							if(Val_Insulin <= 0) {
								insBool = false;

							}
						}//when both are finished in next iteration this condition should not be satisfied
						if(mealBool==false && insBool==false) {
							mealTime = false;
						}// GLUCAGON activity must start here.
						 // if physical activities happen the glucose level starts droping
					}else if(physicalBool==true) {
						if(physicalActivities > 0) {// if activities are going on and level keeps going down continuesly.
							physicalTemp = physicalActivities* 3/40;
							physicalActivities = physicalActivities - physicalTemp; 
							Curr_BGL=Curr_BGL*0.9999-physicalTemp;
						}// if activities are finished then glucose level completely drops and remains stable
						else {
							physicalBool = false;
						}// if glucagon injection is not completely finished
						if(glucagonBool== true){
							gDoseTemp = glucagonDose/20;
							glucagonDose = glucagonDose - gDoseTemp;
							Curr_BGL=Curr_BGL*0.9999+gDoseTemp-physicalTemp;	
						}
					}
					// if nothing is given given or taken, and when the body is functioning normally 
					else {
						Curr_BGL= Curr_BGL*0.999756;
						// if bgl level exceeds 108, then it reduces by 0.14% of the current one.
						if(Curr_BGL>108) {
							Curr_BGL= Curr_BGL*0.99855;
						}// if bgl level falls bellow 90, then it increases by 6.9% of the current one.
						else if(Curr_BGL<90) {
							Curr_BGL= Curr_BGL*1.06923;
						}
						else if(Curr_BGL<108 && Curr_BGL>90) {
							Curr_BGL= Curr_BGL*0.99855;
						}
					}

					DecimalFormat f = new DecimalFormat("##.000");
					Platform.runLater(new Runnable() 
					{
						@Override 
						public void run() 
						{
							System.out.println(f.format(Curr_BGL));
						}
					});

					// This makes the graph of the obtained glucose values
					Data<String,Number> CurrentData = new Data<String, Number>(LocalDateTime.now().toString().substring(11, 19), Curr_BGL);
					actualValueSeries.getData().add(CurrentData);
					InsulinThreshold.getData().add(new Data<String, Number>(LocalDateTime.now().toString().substring(11, 19), 180));
					GlucagonThreshold.getData().add(new Data<String, Number>(LocalDateTime.now().toString().substring(11, 19), 40));

					// This maintains exactly 25 points on the X-axis
					if(actualValueSeries.getData().size() >25)
					{
						actualValueSeries.getData().remove(0, 1);
					InsulinThreshold.getData().remove(0, 1);
					GlucagonThreshold.getData().remove(0, 1);
					}

					// This turns on the alarm when the glucose Level falls in the danger zone of Curr_BGL <= 40 or Curr_BGL >= 180 mg/dL
					if(Curr_BGL>=180 || Curr_BGL <= 40) {


						if(Curr_BGL>180)
							DisplayText.setText("Glucose level is above normal level!!!");
						if(Curr_BGL<40)
							DisplayText.setText("Glucose level is critically low!!! Please inject Glucagon");
						DisplayText.setStyle("-fx-text-inner-color: red;");
						DisplayText.setAlignment(Pos.CENTER);
						if(ChangeSize)
						{
						DisplayText.setFont(Font.font ("Verdana", 12));
						InjInsulin.setFont(Font.font ("Verdana", 12));
						InjGlucagon.setFont(Font.font ("Verdana", 12));
						InjInsulin.setVisible(true);
						InjGlucagon.setVisible(true);
						}
						else
						{
						DisplayText.setFont(Font.font ("Verdana", 20));
						if(Curr_BGL>180)
						{
						InjInsulin.setFont(Font.font ("Verdana", 16));
						InjGlucagon.setVisible(false);
						}
						else
						{
						InjGlucagon.setFont(Font.font ("Verdana", 16));
						InjGlucagon.setVisible(false);
						}
						}
						ChangeSize = !ChangeSize;
						notify_ON= true;

					}
					// This turns off the alarm when the glucose Level is not in the danger zone anymore,  40 < Curr_BGL < 180 mg/dL
					if( Curr_BGL<180 && Curr_BGL >40) {
						InjGlucagon.setVisible(true);
						InjInsulin.setVisible(true);
						InjGlucagon.setFont(Font.font ("Verdana", 12));
						InjInsulin.setFont(Font.font ("Verdana", 12));
						DisplayText.setText("Glucose level is normal!!! Keep it Up :)");
						DisplayText.setFont(Font.font ("Verdana", 12));
						DisplayText.setStyle("-fx-text-inner-color: green;");
						DisplayText.setAlignment(Pos.CENTER);
					}
					int DBCounter = 0;
					if(DBCounter % DBInsertInterval==0) {
						System.out.println("in if.DBCounter..."+DBCounter);
					PatientHistory patientHistory = new PatientHistory(Curr_BGL,PatientID);
					DBCaller.InsertPatientHistory(patientHistory);
					}
					DBCounter++;
					// this sets the pump to the exact insulin remaining
					if(insBool==true && Val_Insulin >0 ) {
						// to increase the capacity of the tank divide by a factor
						Ins_Pump_Val=(((Ins_ref-Val_Insulin)/50)/ins_disp)/2;
						if(Val_Insulin==0) {
							Total_Ins_Pump_Val=Total_Ins_Pump_Val+Ins_Pump_Val;
						}
						BatteryIndicator.setProgress(refInsPB-(Ins_Pump_Val-Total_Ins_Pump_Val)/100);
						refInsPB = refInsPB-(Ins_Pump_Val-Total_Ins_Pump_Val)/100;
					}	
					// this sets the pump to the exact glucagon remaining
					if(glucagonBool==true && glucagonDose >0 ) {
						// to increase the capacity of the tank divide by a factor
						Gluc_Pump_Val=(((gluc_ref-glucagonDose)/50)/gluc_disp)/2;
						if(glucagonDose==0) {
							Total_Gluc_Pump_Val=Total_Gluc_Pump_Val+Gluc_Pump_Val;
						}
					    GlucagonIndicator.setProgress((refGlucPB-(Gluc_Pump_Val-Total_Gluc_Pump_Val)/100));
						refGlucPB = refGlucPB-(Gluc_Pump_Val-Total_Gluc_Pump_Val)/100;
					}
					refferenceBatery = refferenceBatery - (1-refInsPB)/10-(1-refGlucPB)/10 -counter/1200;
					BatteryIndicator.setProgress(refferenceBatery);
					bglValue.setText((f.format(Curr_BGL)));
					Thread.sleep(1000);
				}
				counter++;
			}
		}
		catch(Exception e) {

		}
	}

	@Override
	public void setViewParent(ViewsController viewPage) { //
		// TODO Auto-generated method stub
		controller =viewPage;
	}
	
	

}




